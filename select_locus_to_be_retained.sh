#!/bin/bash

# Locus à garder suivant 3 possibilités :

# 1. Pour chaque loci le plus long (default)
select_the_longest() {
   fastalength all_seq_locus.fasta > temp_length
   grep ">" all_seq_locus.fasta | awk -F "_" '{print $1"_"$2"_"}' \
        | sed -e 's/>//g' | sort | uniq  > temp_list_loci
   
   while read a ; do 
   	grep "$a" temp_length | head -n 1 | awk '{print $2}' >> temp_list_longest ;
   done < temp_list_loci
   
   fastaselect.pl all_seq_locus.fasta temp_list_longest > all_seq_locus_uniq.fasta

   #rm temp_length temp_list_loci temp_list_longest
}

# 2. Le probe avec le plus de loci
select_max_loci () {
   echo "select_max_loci"
   probe=`grep ">" all_seq_locus.fasta | cut -d "_" -f 3- | sort | uniq -c | sort -rn -k 1 | head -n 1 | awk '{print $2}'`
   
   grep -A 1 "$probe" all_seq_locus.fasta > all_seq_locus_uniq.fasta
}

# 3. Probe spécifié par l'utilisateur
select_user_defined () {
   probe=$1
   grep -A 1 "$probe" all_seq_locus.fasta > all_seq_locus_uniq.fasta
}

# main
if [ "$#" -ne 1 ]; then
    echo "$0: Illegal number of parameters"
    exit 1
else
    CHOICE=$1
fi

case $CHOICE in
    "default")   select_the_longest;;
    "max-loci")  select_max_loci;;
    *)           select_user_defined $CHOICE;;
esac

