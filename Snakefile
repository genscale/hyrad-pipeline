"""
Usage:
-----
   conda env update --file environment.yaml
   conda activate hyrad-pipeline

   snakemake --dryrun --printshellcmds --cores 4
   snakemake          --printshellcmds --cores 4

   snakemake --dag | dot -Tsvg > dag.svg && open -a Safari dag.svg
   time snakemake -p -j 72 2>&1 > snakemake.output
"""

import subprocess
import logging
from pprint import pformat
import glob
from pathlib import Path
import sys
import json

#-- Define snakemake config file
configfile: "config-smk-temp.yml"

logging.info("*** Snakemake config:\n"+pformat(config))

#-- Initiate logging
log_format = '%(asctime)s | %(name)s | %(levelname)s | %(message)s'

logging.basicConfig(filename='popHyRAD_pipeline.log',
                    encoding='utf-8',
                    level=(logging.DEBUG if config["debug"] else logging.INFO),
                    format=log_format,
                    datefmt='%Y-%m-%d %H:%M:%S') # seconds with 3 decimals are not needed

#-- Path of input files
logging.info("*** Required input data (Probes):"+config["sorted_fastq_path"])
logging.info("*** Required input data (Samples):"+"sample_reads/*_R?.*")

#-- List of input files
Probes_input_files = glob.glob(config["sorted_fastq_path"]) # wildcard expansion
logging.info("*** Input files list (Probes_input_files):\n"+'\n '.join(Probes_input_files))

Samples_input_files = glob.glob("sample_reads/*_R?.*") # wildcard expansion
logging.info("*** Input files list (Samples_input_files):\n"+'\n '.join(Samples_input_files))

#-- Build list of Probes R1 files, only their basename, without extension (ex. HLPR3_L1_R1_001)
Probes_input_files_R1=[fic for fic in Probes_input_files if "_R1" in fic]
Probes_input_files_R1_basename=[os.path.basename(fic).replace('.fastq.gz','') for fic in Probes_input_files if "_R1" in fic]
logging.debug("*** Probes_input_files_R1_basename:\n"+'\n '.join(Probes_input_files_R1_basename))

#-- Build list of Sample names
def get_filename_without_extension(file_path):
    file_basename = os.path.basename(file_path)
    filename_without_extension = file_basename.split('.')[0]
    return filename_without_extension

Samples_names=[get_filename_without_extension(fic).split('_')[0] for fic in Samples_input_files]
Samples_names=sorted(list(set(Samples_names)))  # remove duplicates from R1 R2
logging.debug("*** Samples names:\n"+'\n '.join(Samples_names))

#-- Prepare ipyrad params file
f_input  = open(config["ipyrad_params_tmpl_file"],'r')
f_output = open('params-temp.txt','w')

assembly_name = config["assembly_name"] # global variable (needed several times)
loci_dir = assembly_name+"_outfiles"
to_replace = { 'ASSEMBLY_NAME'        :             assembly_name,
               'SORTED_FASTQ_PATH'    :     config["sorted_fastq_path"],
               'MINDEPTH_STATISTICAL' : str(config["mindepth"]),
               'MINDEPTH_MAJRULE'     : str(config["mindepth"]),
               'CLUST_THRESHOLD'      : str(config["clust_threshold"]),
               'MIN_SAMPLES_LOCUS'    : str(config["min_samples_locus"]),
             }

logging.info("*** to_replace :\n"+pformat(to_replace))

for line in f_input:
    for k, v in to_replace.items():
        line = line.replace(k,v)
    f_output.write(line)

f_output.close()  # ipyrad params file ready to use
f_input.close()

#--Pipeline rules

rule all:
    input:
       assembly_name+"_outfiles/"+assembly_name+".loci",
       loci_dir+"/"+assembly_name+".loci",
       loci_dir+"/all_seq_locus.fasta",
       loci_dir+"/all_seq_locus_uniq.fasta",
       loci_dir+"/sample_mapping_ok",
       loci_dir+"/log_file_ok",
       loci_dir+"/snpset_freebayes.vcf"
       
#############################################################################
# 1. Probes part                                                            #
#############################################################################

#--ipyrad process
rule ipyrad:
    params:
        steps_list = config["steps_list"],
    output:
        loci_dir+"/"+assembly_name+".loci"   # run_ipyrad.loci
    shell:
        "ipyrad -p params-temp.txt -s {params.steps_list} --results"

#-- Transform ipyrad output: .loci -> .fasta
rule loci2fasta:
    input:
        loci_dir+"/"+assembly_name+".loci"
    output:
        loci_dir+"/all_seq_locus.fasta"
    run:
        shell(
            "cd "+loci_dir+";"
            "../loci2fasta.sh "+assembly_name+".loci;"
        )
        # Number of sequences in the reference catalog (grep -c ">" all_seq_locus.fasta)
        cmd='grep -c ">" '+loci_dir+'/all_seq_locus.fasta'
        seq_number = subprocess.check_output(cmd, shell=True, text=True)
        logging.info("*** Number of sequences in the reference catalog : "+seq_number)
        
#--Locus to be retained (3 possibilities)
rule select_locus_to_be_retained:
    input:
        loci_dir+"/all_seq_locus.fasta"
    output:
        loci_dir+"/all_seq_locus_uniq.fasta"
    shell:
       "cd "+loci_dir+"; ../select_locus_to_be_retained.sh "+config["probe"]

#############################################################################
# 2. Sample mapping                                                         #
#############################################################################

sample_mapping_output_files=[f'{loci_dir}/{sample_name}_sorted_keep_pcrdup.rescaled.bam' 
                 for sample_name in Samples_names]

logging.debug("*** List of expected output files from the 'sample mapping' section:\n"+'\n '.join(sample_mapping_output_files))

rule sample_mapping:
    input:
        Samples_input_files,
        loci_dir+"/all_seq_locus_uniq.fasta"
    output:
        sample_mapping_output_files,
        loci_dir+"/all_seq_locus_2.fasta",
        loci_dir+"/number_of_reads.json",
        loci_dir+"/sample_mapping_ok"
    shell:
        "cd "+loci_dir+"; ../sample_mapping.sh "+config["mapping_algorithm"]

#############################################################################
# 3. Update log file
#############################################################################
# snakemake --printshellcmds --cores 4 --force update_log_file

rule update_log_file:
    input:
        sample_mapping_output_files,
        loci_dir+"/number_of_reads.json",
        loci_dir+'/all_seq_locus.fasta'
    output:
        loci_dir+"/log_file_ok" 
    run:
        logging.info("==============================================================================")
        logging.info("  Pipeline summary: ")
    
        #--Summarize user specified (command line) options -n -d -c -s
        logging.info("User defined parameters : ")
        logging.info("  -n -> name (default run_ipyrad)                     : "+config["assembly_name"])
        logging.info("  -d -> min depth (default 6)                         : "+str(config["mindepth"]))
        logging.info("  -c -> % identify  (no default need to be specified) : "+str(config["clust_threshold"]))
        logging.info("  -s -> min samples sharing loci (default 2)          : "+str(config["min_samples_locus"]))

        #--Number of sequences in the reference catalog
        #    grep -c ">"              all_seq_locus.fasta
        cmd='grep -c ">" '+loci_dir+'/all_seq_locus.fasta'
        res = subprocess.check_output(cmd, shell=True, text=True)
        logging.info("Number of sequences in the reference catalog : "+res)
    
        #--Number of reads in each sample (read JSON written by sample_mapping.sh)                
        with open(loci_dir+'/number_of_reads.json') as f:
           data = f.read()
        js = json.loads(data)                     
        logging.info("Number of reads in each sample : \n"+str(js))

        logging.info("==============================================================================")
        shell("touch "+loci_dir+"/log_file_ok")
     
#############################################################################
# 4. SNP calling
#############################################################################

rule snp_calling:
    input:
        loci_dir+"/all_seq_locus_2.fasta",
        sample_mapping_output_files
    output:
        loci_dir+"/snpset_freebayes.vcf"
    shell:
        "freebayes -f "+loci_dir+"/all_seq_locus_2.fasta {sample_mapping_output_files} > "+loci_dir+"/snpset_freebayes.vcf"

#############################################################################
# Cleaning                                                                  #
#############################################################################

rule clean:
    shell:
        "rm -rf "+assembly_name+"* params-temp.txt sag.svg"

