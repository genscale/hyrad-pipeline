# popHyRAD pipeline

## Prepare your environement

Create the following environment (this may take a while):

	conda env create --file environment.yaml
	
And then activate it:

	conda activate hyrad-pipeline
	
	
## Run the pipeline with the companion data (small dataset)

Launch the following command:
    
	# have a look at possible options and default values
	python run_hyrad_pipeline.py --help
	
	# launch the pipeline (remove --dry-run below)
	python run_hyrad_pipeline.py pop --debug --mapping-algorithm flexible --dry-run
	
