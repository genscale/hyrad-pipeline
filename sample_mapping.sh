#!/bin/bash
set -xv

SAMPLES_R1_list=($(ls ../sample_reads/*_R1.f*)) # create an array with all reads files
#echo SAMPLES_R1_list : ${SAMPLES_R1_list[@]}
printf "Number of samples : %s\n" ${#SAMPLES_R1_list[*]}

#--Useful for debugging (when launching sample_mapping.sh directly)
for FILE in temp.sam all_seq_locus_2.dict number_of_reads.json; do
    [ -f $FILE ] && { echo "Removing $FILE"; rm $FILE; }
done

#--N in upper case, remove gap (- and space) to fit mapping requirements
sed -e 's/n/N/g' \
    -e 's/ //g' \
    -e 's/-/N/g' all_seq_locus_uniq.fasta > all_seq_locus_2.fasta

#--Reference indexing
bwa index all_seq_locus_2.fasta

MAPPING_ALGORITHM=$1 # {strict,flexible}

printf '{\n' >> number_of_reads.json

################################################################
# Loop over R1 files                                           #
counter=1                                                      #
comma=,                                                        #
for FILE in ${SAMPLES_R1_list[@]}; do                          #
################################################################

    [ $counter -eq ${#SAMPLES_R1_list[@]} ] && { comma= ; } # no comma after last json record

    printf "Counter : %s - comma : %s\n" $counter $comma
    
    echo
    echo "======================================================================"
    echo "Handling ${FILE}..."
    echo "======================================================================"
    echo
    
    #--File name manipulations
    # cf. https://tldp.org/LDP/LG/issue18/bash.html
    #                                         # ex. FILE=../sample_reads/SAMPLE1_R1.fastq.gz
    input_path=${FILE%/*}                     # ../sample_reads
    file=${FILE##*/}                          # SAMPLE1_R1.fastq.gz
    base=${file%%.*}                          # SAMPLE1_R1
    ext=${file#*.}                            # fastq.gz

    sample=`echo $base | sed -e 's/_R1//g'`   # ex. SAMPLE1
    
    R1_sample_file=$FILE
    R2_sample_file=`echo $FILE | sed -e 's/_R1/_R2/g'`

    #---------------------------# 
    #     Mapping algorithm     #
    #---------------------------# 

    case $MAPPING_ALGORITHM in
	strict)
	    echo "=====> Mapping algorithm : strict"
	    bwa aln -t 20 all_seq_locus_2.fasta $R1_sample_file  > "$sample"_aln_R1.sai
	    bwa aln -t 20 all_seq_locus_2.fasta $R2_sample_file  > "$sample"_aln_R2.sai
	    bwa sampe all_seq_locus_2.fasta \
		"$sample"_aln_R1.sai "$sample"_aln_R2.sai \
		$R1_sample_file $R2_sample_file  > temp.sam
	    ;;
	flexible)
	    #TODO ? n'utilise qu'un coeur, est-ce parallélisable?
	    echo "=====> Mapping algorithm : flexible"
	    bwa mem all_seq_locus_2.fasta \
		$R1_sample_file $R2_sample_file  > temp.sam
	    ;;
	*)
	    echo "Unknown mapping algorithm" ; exit 1;
	    ;;
    esac
    
    #---------------------------# 
    #          Cleaning         #
    #---------------------------#
    
    #--Sort mapping
    samtools sort temp.sam -o temp_1_sorted.bam

    #--Number of mapped reads before cleaning (for JSON output)
    nb_mapped_reads_before_cleaning=`samtools flagstat temp.sam | grep "mapped (" | grep -v primary | awk '{print $1}'`
    rm temp.sam 

    #--Number of reads in each sample (for JSON output)
    nb_raw_reads_R1=$((`grep -c "." ${R1_sample_file}`/4))
    nb_raw_reads_R2=$((`grep -c "." ${R2_sample_file}`/4))
 
    #--Keep only mapped reads
    samtools view -bF 4 temp_1_sorted.bam > temp_1_sorted_keep.bam
    rm temp_1_sorted.bam
    
    #--Index mapping
    samtools faidx all_seq_locus_2.fasta

    #--Dictionnary
    picard CreateSequenceDictionary -R all_seq_locus_2.fasta -O all_seq_locus_2.dict # must stay inside the loop
    
    #--Read groups
    picard AddOrReplaceReadGroups -I temp_1_sorted_keep.bam -O temp_1_sorted_keep_rg.bam -ID ["$sample"] -RGLB [id] -PL [pl] -PU [pu] -SM ["$sample"]

    #--Index bam file
    samtools index temp_1_sorted_keep_rg.bam
    
    #--Indel realignment (gatk)
    GenomeAnalysisTK -T RealignerTargetCreator -I temp_1_sorted_keep_rg.bam -R all_seq_locus_2.fasta -o temp.intervals
    GenomeAnalysisTK -T IndelRealigner         -I temp_1_sorted_keep_rg.bam -R all_seq_locus_2.fasta -targetIntervals temp.intervals -o temp_1_sorted_keep_rg_realign.bam

    #--Remove pcr duplicates
    picard  MarkDuplicates -I temp_1_sorted_keep_rg_realign.bam -O temp_1_sorted_keep_pcrdup.bam -M marks -REMOVE_DUPLICATES true
    
    #--Mapping correction with mapdamage
    time mapDamage -i temp_1_sorted_keep_pcrdup.bam -r all_seq_locus_2.fasta --rescale --merge-reference-sequences
    
    #--Give good name to output file
    cp ./results_temp_1_sorted_keep_pcrdup/temp_1_sorted_keep_pcrdup.rescaled.bam ./"$sample"_sorted_keep_pcrdup.rescaled.bam
    
    #--Cleaning before handling next sample
    rm all_seq_locus_2.dict

    #--Number of mapped reads after cleaning (for JSON output)
    nb_mapped_reads_after_cleaning=`samtools flagstat "$sample"_sorted_keep_pcrdup.rescaled.bam | grep "mapped (" | grep -v primary | awk '{print $1}'`

    #--Write JSON output
    printf '"%s" : [{ "nb_raw_reads_R1" : "%s", "nb_raw_reads_R2" : "%s", "nb_mapped_reads_before_cleaning" : "%s", "nb_mapped_reads_after_cleaning" : "%s"}]%s\n' \
	   $sample $nb_raw_reads_R1 $nb_raw_reads_R2 $nb_mapped_reads_before_cleaning $nb_mapped_reads_after_cleaning $comma \
	   >> number_of_reads.json
    
    echo
    echo "Handling ${FILE} finished..."
    echo
    
################################################################
    (( counter = counter + 1 ))                                #
done                                                           #
################################################################

printf '}\n' >> number_of_reads.json

js-beautify --replace number_of_reads.json # not compulsory

#######################
#######################
touch sample_mapping_ok
#######################
#######################
