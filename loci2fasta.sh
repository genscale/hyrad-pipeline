#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "$0: Illegal number of parameters"
    exit 1
fi

INPUT_LOCI_FILE=$1

cat $INPUT_LOCI_FILE | tr '\n' '\t' | tr '|' '\n' | grep "/" | sed -e 's/^\t//g' | awk '{print NR"\t"$0}' > temp1

while read a ; do 
	name=`echo "$a" | awk '{print $1}'` ;
	echo "$a" | tr '\t' '\n' | awk '{print ">locus_"'$name'"_"$0}' | tr ' ' '\n' | grep "." | grep "T" -B 1 >> all_seq_locus.fasta ;
done < temp1
