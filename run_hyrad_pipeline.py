"""
Description:
  **************************
  ** Pipeline using hyrad **
  **************************

  ===> Probes data
  The "probe_reads" directory must exist in the same directory as this script.
  Naming convention : file names must include _R1_ and _R2_ (e.g. [probe_name]_R1_.fastq.gz [probe_name]_R2_.fastq.gz).

  ===> Samples data
  The "samples_reads" directory must exist in the same directory as this script.
  Naming convention: file names must include _R1 and _R2

  ===> How to run the pipeline with companion data (provided small dataset)?

  # You first need to bootstrap the "hyrad-pipelinee" CONDA environment
  conda env update --file environment.yaml
  conda activate hyrad-pipeline

  #========================= (remove --dry-run below) =============================
  python run_hyrad_pipeline.py pop --debug --mapping-algorithm flexible --dry-run
  #================================================================================

Usage:
  run_hyrad_pipeline.py pop   [--assembly-name <name>]
                              [--min-depth <int>]
                              [--clustering-threshold <percentage>]
                              [--min-samples-sharing-loci <int>]
                              [--mapping-algorithm <algo>]
                              [--locus-to-keep <locus>]
                              [--threads <int>]
                              [--dry-run] 
                              [--debug]
  run_hyrad_pipeline.py phylo

Options: 
  -h --help
  -n --assembly-name <name>               Assembly name. Used to name output directories for assembly steps. [default: run_ipyrad]
  -d --min-depth <int>                    Min depth for 'statistical base' & for 'majority-rule' base calling. [default: 6]
  -c --clustering-threshold <percentage>  Clustering threshold for de novo assembly (% identify). [default: 0.8]
  -s --min-samples-sharing-loci <int>     Min # samples per locus for output. [default: 2]
  --threads <int>                         Number of threads. [default: 4]
  --dry-run                               
  --debug                                 
  --mapping-algorithm <algo>              strict or flexible [default: strict]
  --locus-to-keep <locus>                 Locus to keep, 3 possibilities : select_the_longest, select_max_loci, or user defined probe (see select_locus_to_be_retained.sh) [default: select_the_longest]

"""

import sys
import docopt
import pprint
import os
import re
import time

args = docopt.docopt(__doc__)
pprint.pprint(args)

##############################################################
# Check input data
##############################################################

#--Check probe_reads data (./probe_reads/PROBE*_R?_*)
if not os.path.exists("probe_reads"):
    sys.exit("(!) probe_reads diretory doesn't exist. We exit.")

# *TODO* input files synchronisés à vérifier (R1 et R2 correspondants dans le même ordre)
    
#--Check sample_reads data (sample_reads/*_R?.*)
if not os.path.exists("sample_reads"):
    sys.exit("(!) sample_reads diretory doesn't exist. We exit.")

# *TODO* vérifier R1 et R2
    
##############################################################
# Prepare snakemake config file
##############################################################

to_replace=[
    ("^assembly_name:.*",     f'assembly_name:     {args["--assembly-name"]}'            ),
    ("^mindepth:.*",          f'mindepth:          {args["--min-depth"]}'                ),
    ("^clust_threshold:.*",   f'clust_threshold:   {args["--clustering-threshold"]}'     ),
    ("^min_samples_locus:.*", f'min_samples_locus: {args["--min-samples-sharing-loci"]}' )
]

print(to_replace)

with open ('config-smk-tmpl.yml', 'r' ) as f_in:
    print("------------")
    content_update = f_in.read()
    for regex,args_value in to_replace:
        content_update = re.sub(regex, args_value, content_update, flags = re.M)    

content_append=f'''
mapping_algorithm: {args["--mapping-algorithm"]}
locus_to_keep:     {args["--locus-to-keep"]}
debug:             {args["--debug"]}
'''
print(content_append)
        
with open('config-smk-temp.yml','w') as f_out:  # overwrite if file exists
    f_out.write(content_update)
    f_out.write(content_append)
    
##############################################################
# Build snakemake command and launch snakemake
##############################################################

#--Build the snakemake command
cmd_smk = f'snakemake --printshellcmds --cores {args["--threads"]}'
cmd_smk += (" --dryrun" if args["--dry-run"] else "")

print(cmd_smk)

start_time = time.time()
os.system(cmd_smk)
end_time = time.time()
print(f"Run duration {(end_time-start_time)/60} minutes")
